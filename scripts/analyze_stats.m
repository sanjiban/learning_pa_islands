clc;
clear;
close all;

%% Create an environment
folder = '/home/aeroscout/data/learning_heursitics/set6/';
stats_folder = strcat(folder, 'stats');
sol_folder = strcat(folder, 'solutions');

num_trials = 999;

planning_time = [];
expansions = [];
solution_cost = [];
for i = 1:num_trials
    filename = strcat(stats_folder, '/stats', sprintf('%03d',i), '.txt');
    info = dir(filename);
    
    sol_filename = strcat(sol_folder, '/sol', sprintf('%03d',i), '.txt');
    sol_info = dir(sol_filename);

    if (isempty(info) || info.bytes == 0 || isempty(sol_info) || sol_info.bytes == 0)
        continue;
    end
    stats = dlmread(filename);
    
    planning_time = [planning_time stats(1)];
    expansions = [expansions stats(2)];
    solution_cost = [solution_cost stats(3)];
end

disp('mean planning time')
mean(planning_time)
disp('mean expansions')
mean(expansions)
disp('mean solution cost')
mean(solution_cost)