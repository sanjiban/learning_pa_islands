clc;
clear;
close all;

%% Create an environment
map_name = '../maps/sharp_turn_med.txt';
filename = '../cfgs/sharp_turn_med.cfg';

start = [1 5 0]; %[0.1 0.375 0];
goal = [9 5 0]; %[0.625 0.375 0];

map = dlmread(map_name);
resolution = 0.025;

% Generate cfgs
generate_cfg( map, resolution, start, goal, filename );

visualize_map( map, resolution );