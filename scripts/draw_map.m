clc;
clear;
close all;

%% Create drawing platforn 
bbox = [0 10 0 10];
resolution = 0.025; %resolution of map

figure;
axis(bbox);
axis xy;
grid on;
count = 1;
while(1)
    title('Drag rect');
    final_rect = getrect(gca);
    rectangle('Position',final_rect,'FaceColor','r');
    rectangle_array(count).low = final_rect(1:2);
    rectangle_array(count).high = final_rect(1:2)+final_rect(3:4);
    
    rectangle_array(count).low = wrev(rectangle_array(count).low);
    rectangle_array(count).high = wrev(rectangle_array(count).high);

    map = rectangle_maps( bbox, rectangle_array, resolution);
    
    clf;
    visualize_map(map, resolution);
    grid on;

    count = count + 1;
    title('Left click to continue, right click to stop');
    [~,~,button] = ginput(1);
    if (button == 3)
        break;
    end
end
