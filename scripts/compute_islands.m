clc;
clear;
close all;

%% Create an environment
folder = '/home/aeroscout/data/learning_heursitics/set3/';
cfg_folder = strcat(folder, 'cfgs');
sol_folder = strcat(folder, 'solutions');

%% Load map
map_name = '../maps/sharp_turn_med.txt';
num_trials = 999;
map = dlmread(map_name);
resolution = 0.025;

figure;
visualize_map(map, resolution);
hold on;

% Heatmap
hm = zeros(20,20);
hm_resolution = (size(map,1)/size(hm,1))*resolution;
world_wp_to_rc = @(xy) ceil(xy/hm_resolution);
rc_to_world_wp = @(r, c) [r*hm_resolution c*hm_resolution];
% Point set
point_set = [];

filter = 1;

for i = 1:num_trials
    filename = strcat(sol_folder, '/sol', sprintf('%03d',i), '.txt');
    info = dir(filename);
    if (isempty(info) || info.bytes == 0)
        continue;
    end
    path = dlmread(filename);

    xy = path(:,1:2);
    
    hm_current = zeros(size(hm));
    rc = world_wp_to_rc(xy);
    hm_current(sub2ind(size(hm), rc(:,1), rc(:,2))) = 1;
    hm_current = min(ones(size(hm)), hm_current);
    hm = hm + hm_current;
    
    [r, c] = ind2sub(size(hm_current), find(hm_current == 1));
    xy_filt = rc_to_world_wp(r,c);
    
    if (~filter)
        %point_set = [point_set; xy_filt];
    %else
        point_set = [point_set; xy];
    end
    
    plot(xy(:,1), xy(:,2));
end

hm = hm / sum(sum(hm));

%% Cluster
if (filter)
    %cluster_centre = MeanShiftCluster(point_set',10*hm_resolution);
    %cluster_centre = cluster_centre';
    [XMAX,IMAX,XMIN,IMIN] = extrema2(hm);
    [r, c] = ind2sub(size(hm), IMAX);
    cluster_centre = [r c]*hm_resolution;
    cluster_centre = cluster_centre(1:2, :);
else
    cluster_centre = MeanShiftCluster(point_set',10*resolution);
    cluster_centre = cluster_centre';
end

disp('Islands are');
cluster_centre

%% Plot hm
figure;
hold on;
colormap(parula);
colorbar
imagesc([0 hm_resolution*size(hm,1)], [0 hm_resolution*size(hm,2)], hm');
axis xy;
axis([0 hm_resolution*size(hm,1) 0 hm_resolution*size(hm,2)]); 
plot(cluster_centre(:,1), cluster_centre(:,2),'.r','markersize', 50);
