clc;
clear;
close all;

%% Create an environment
folder = '/home/aeroscout/data/learning_heursitics/set6/';
cfg_folder = strcat(folder, 'cfgs');
sol_folder = strcat(folder, 'solutions');
stats_folder = strcat(folder, 'stats');
mprim_filename = strcat(folder, 'mprims/unicycle_noturninplace.mprim');
islands_filename = strcat(folder, 'islands/2islands.txt');
script_filename = strcat(folder, 'run_experiment.sh');


map_name = '../maps/sharp_turn_med.txt';
resolution = 0.025;
num_trials = 999;


start_bb = [0  0 0;
            10 10 2*pi];
end_bb = start_bb;
      

%% Generate cfgs
map = dlmread(map_name);

for i = 1:num_trials
    start = start_bb(1,:) + rand(1,3).*diff(start_bb);
    goal = end_bb(1,:) + rand(1,3).*diff(end_bb);
    
    cfg_filename = strcat(cfg_folder, '/env', sprintf('%03d',i), '.cfg');

    generate_cfg( map, resolution, start, goal, cfg_filename );
end

%% Generate experiments
fid = fopen(script_filename, 'w');
fprintf(fid, '#!/bin/bash \n\n');
for i = 1:num_trials
    fprintf(fid, 'rosrun sbpl test_pa_islands %s/env%03d.cfg %s %s %s/sol%03d.txt --visualization=false --stats_file=%s/stats%03d.txt \n', cfg_folder, i, mprim_filename, islands_filename, sol_folder, i, stats_folder, i);
end
fclose(fid);
