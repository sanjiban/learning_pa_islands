clc;
clear;
close all;

%% Create an environment
cfg_filename = '../cfgs/sharp_turn_med.cfg';
sol_filename = '../solutions/no_heuristic.txt';

[map, resolution] = read_cfg(cfg_filename);

visualize_map( map, resolution );
hold on;
path = dlmread(sol_filename);
plot(path(:,1), path(:,2), 'r');