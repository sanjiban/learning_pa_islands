clc;
clear;
close all;

%% Create an environment
map_name = 'maps/sharp_turn.txt';
sol_name = 'sol_simple.txt';
%% Generate cfgs
map = dlmread(map_name);
resolution = 0.025;

figure;
hold on;
colormap(gray);
imagesc([0 resolution*size(map,1)], [0 resolution*size(map,2)], ~map);
axis xy;
axis([0 resolution*size(map,1) 0 resolution*size(map,2)]); 

path = dlmread(sol_name);
xy = path(:,1:2);
plot(xy(:,1), xy(:,2));