function plot_path( cfg_filename, sol_filename )
%PLOT_PATH Summary of this function goes here
%   Detailed explanation goes here


[map, resolution] = read_cfg( cfg_filename );
path = dlmread(sol_filename);

visualize_map( map, resolution );
hold on;
plot(path(:,1), path(:,2), 'r');
end

