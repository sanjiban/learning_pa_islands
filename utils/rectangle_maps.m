function [ map ] = rectangle_maps( bbox, rectangle_array, resolution)

world_to_grid = @(xy) ceil(xy/resolution);

map = zeros( ceil(bbox(2)/resolution), ceil(bbox(4)/resolution) );
for i = 1:length(rectangle_array)
    rc1 = world_to_grid([rectangle_array(i).low(1) rectangle_array(i).low(2)]);
    rc2 = world_to_grid([rectangle_array(i).high(1) rectangle_array(i).high(2)]);
    row1 = min(size(map,1), max(1, rc1(1)));
    row2 = min(size(map,1), max(1, rc2(1)));
    col1 = min(size(map,2), max(1, rc1(2)));
    col2 = min(size(map,2), max(1, rc2(2)));
    map(row1:row2, col1:col2) = 1;
end

end
