function [map, resolution] = read_cfg( filename )
%READ_CFG Summary of this function goes here
%   Detailed explanation goes here

fid = fopen(filename, 'r');
fscanf(fid, 'discretization(cells): %d %d\n');
fscanf(fid, 'obsthresh: 1\n');
fscanf(fid, 'cost_inscribed_thresh: 1\n');
fscanf(fid, 'cost_possibly_circumscribed_thresh: 0\n');
resolution = fscanf(fid, 'cellsize(meters): %f\n')
fscanf(fid, 'nominalvel(mpersecs): 1.0\n');
fscanf(fid, 'timetoturn45degsinplace(secs): 2.0\n');
fclose(fid);

map = dlmread(filename, ' ', 10, 0);
size(map)
end

