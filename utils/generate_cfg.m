function generate_cfg( map, resolution, start, goal, filename )
%GENERATE_CFG Summary of this function goes here
%   Detailed explanation goes here

fid = fopen(filename, 'w');
fprintf(fid, 'discretization(cells): %d %d\n', size(map,1), size(map,2));
fprintf(fid, 'obsthresh: 1\n');
fprintf(fid, 'cost_inscribed_thresh: 1\n');
fprintf(fid, 'cost_possibly_circumscribed_thresh: 0\n');
fprintf(fid, 'cellsize(meters): %f\n', resolution);
fprintf(fid, 'nominalvel(mpersecs): 1.0\n');
fprintf(fid, 'timetoturn45degsinplace(secs): 2.0\n');
fprintf(fid, 'start(meters,rads): %0.2f, %0.2f, %0.2f\n', start(1), start(2), start(3));
fprintf(fid, 'end(meters,rads): %0.2f, %0.2f, %0.2f\n', goal(1), goal(2), goal(3));
fprintf(fid, 'environment:\n');
fclose(fid);
dlmwrite(filename, map,'-append', 'delimiter',' ');
end

