function visualize_map( map, resolution )
%VISUALIZE_MAP Summary of this function goes here
%   Detailed explanation goes here

colormap(gray);
imagesc([0 resolution*size(map,1)], [0 resolution*size(map,2)], ~map);
axis xy;
axis([0 resolution*size(map,1) 0 resolution*size(map,2)]); 

end

